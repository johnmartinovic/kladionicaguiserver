﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Xml;

namespace KladionicaDataDownloader
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Globalne varijable
        /// </summary>

        // path do XML dokumenta s postavkama programa
        string XMLPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) +
            "\\KladionicaDATA.xml";

        // // globalne varijable koje su izvucene iz XML-a s postavkama programa
        // pri pokretanju ovog prozora
        string globalPythonPath = "";
        string globalDatabasePath = "";

        private void MainForm_Load(object sender, EventArgs e)
        {
            // poravnanje labela u formu
            this.label1.Left = (int)(this.Width / 2 - label1.Width / 2);

            if (File.Exists(this.XMLPath))
            {
                try
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(this.XMLPath);
                    XmlNodeList DatabasePathNodes = xDoc.SelectNodes("/DataList/Data[@AttName='DatabasePath']");
                    XmlNodeList PythonPathNodes = xDoc.SelectNodes("/DataList/Data[@AttName='PythonPath']");
                    if (DatabasePathNodes.Count == 1 && DatabasePathNodes.Count == 1)
                    {
                        this.globalDatabasePath = DatabasePathNodes[0].InnerText;
                        this.globalPythonPath = PythonPathNodes[0].InnerText;
                        //MessageBox.Show(DatabasePath+" "+PythonPath);
                    }
                }
                catch (XmlException e2)
                {
                    // Maknuti MessageBox u finalnoj verziji zbog Task Schedulera! A i bilo di drugdje
                    MessageBox.Show("Nešto ne valja s XML bazom pathova. Reinicijaliziraj pathove!", "Greška!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            // pokreni skidanje
            Thread T1 = new Thread(HtmlSkidanjePython);
            T1.Start();
        }

        //public string getDatabasePath()
        //{
        //    string temp = "";
        //    this.Invoke(new MethodInvoker(delegate
        //    {
        //        temp = DatabasePath;
        //    }));
        //    return temp;
        //}

        //public string getPythonPath()
        //{
        //    string temp = "";
        //    this.Invoke(new MethodInvoker(delegate
        //    {
        //        temp = PythonPath;
        //    }));
        //    return temp;
        //}
        
        // u nastavku su delegat i metoda koji služe za postavljanje stringa labela ovog prozora
        // iz threada koji skida podatke (kao promjena vrijednosti labela za vrijeme skidanja i to...)
        private delegate void SetLabelSub(string NewText);
        
        private void SetLabel(string NewText)
        {
            if (this.InvokeRequired)
            {
                SetLabelSub Del = new SetLabelSub(SetLabel);
                this.Invoke(Del, new object[] { NewText });
            }
            else
            {
                this.label1.Text = NewText;
                this.label1.Left = (int)(this.Width / 2 - label1.Width / 2);
            }
        }

        /// <summary>
        /// Metoda koja pokrece python skriptu (skripte) koje služe za skidanje
        /// podataka s interneta
        /// </summary>
        public void HtmlSkidanjePython()
        {
            //// uzmi vrijednosti pathova (ovo je thread)
            //string tempPythonPath = getPythonPath();
            //string tempDatabasePath = getDatabasePath();

            //MessageBox.Show(tempDatabasePath + tempPythonPath);
            //MessageBox.Show(this.DatabasePath + this.PythonPath);


            if (!Directory.Exists(this.globalDatabasePath) ||
                !Directory.Exists(this.globalDatabasePath + @"\SupersportHTMLPonuda") ||
                !Directory.Exists(this.globalDatabasePath + @"\SupersportHTMLRezultati") ||
                !Directory.Exists(this.globalDatabasePath + @"\SupersportObradenoPonuda") ||
                !Directory.Exists(this.globalDatabasePath + @"\SupersportObradenoRezultati") ||
                !File.Exists(this.globalPythonPath))
            {
                // Maknuti MessageBox u finalnoj verziji zbog Task Schedulera! A i bilo di drugdje
                MessageBox.Show("Pathovi ne postoje! Valjda si izbrisao foldere. Pokreni ponovno program i spremi postavke.",
                "Greška!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                // zatvori prozor iz ovog threada
                this.Invoke((MethodInvoker)delegate
                {
                    this.Close();
                });

                return;
            }

            string tempPythonKod;
            string tempFile;

            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = this.globalPythonPath; //full path to python.exe

            // pokretanje skidanja HTML-a PONUDA
            this.SetLabel("Skidanje HTML-a PONUDA...");

            tempPythonKod = KladionicaSettings.Properties.Resources.HTMLDownloadPonudaString;
            tempFile = Path.GetTempFileName();
            using (var writer = new StreamWriter(tempFile, false))
                writer.Write(tempPythonKod);

            process.StartInfo.Arguments = "\"" + tempFile + "\"" + " " +
                "\"" + this.globalDatabasePath + @"\SupersportHTMLPonuda" + "\""; //args is path to .py file and any cmd line args

            // MessageBox.Show(process.StartInfo.Arguments);

            process.Start();
            process.WaitForExit();

            // pokretanje skidanja HTML-a REZULTATI
            this.SetLabel("Skidanje HTML-a REZULTATI...");
            tempPythonKod = KladionicaSettings.Properties.Resources.HTMLDownloadRezultatiString;
            tempFile = Path.GetTempFileName();
            using (var writer = new StreamWriter(tempFile, false))
                writer.Write(tempPythonKod);

            process.StartInfo.Arguments = "\"" + tempFile + "\"" + " " +
                "\"" + this.globalDatabasePath + @"\SupersportHTMLRezultati" + "\""; //args is path to .py file and any cmd line args

            // MessageBox.Show(process.StartInfo.Arguments);

            process.Start();
            process.WaitForExit();

            this.SetLabel("GOTOVO");

            // malo zastani da se prikaže da je gotovo
            Thread.Sleep(2000);

            // zatvori prozor iz ovog threada
            this.Invoke((MethodInvoker)delegate
            {
                this.Close();
            });
        }
    }
}
