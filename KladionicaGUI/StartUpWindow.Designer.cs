﻿namespace KladionicaGUI
{
    partial class StartUpWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartUpWindow));
            this.startButton = new System.Windows.Forms.Button();
            this.HTMLponudaBrowseButton = new System.Windows.Forms.Button();
            this.textBoxDBPath = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBoxPythonPath = new System.Windows.Forms.TextBox();
            this.PythonPathBrowseButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.minutePicker = new System.Windows.Forms.NumericUpDown();
            this.satiPicker = new System.Windows.Forms.NumericUpDown();
            this.labelStep1 = new System.Windows.Forms.Label();
            this.labelStep2 = new System.Windows.Forms.Label();
            this.labelStep3 = new System.Windows.Forms.Label();
            this.labelStep4 = new System.Windows.Forms.Label();
            this.picStep4 = new System.Windows.Forms.PictureBox();
            this.picStep3 = new System.Windows.Forms.PictureBox();
            this.picStep2 = new System.Windows.Forms.PictureBox();
            this.picStep1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minutePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.satiPicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep1)).BeginInit();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(176, 147);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(137, 39);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Check all and start!";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // HTMLponudaBrowseButton
            // 
            this.HTMLponudaBrowseButton.Location = new System.Drawing.Point(385, 17);
            this.HTMLponudaBrowseButton.Name = "HTMLponudaBrowseButton";
            this.HTMLponudaBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.HTMLponudaBrowseButton.TabIndex = 0;
            this.HTMLponudaBrowseButton.Text = "Browse...";
            this.HTMLponudaBrowseButton.UseVisualStyleBackColor = true;
            this.HTMLponudaBrowseButton.Click += new System.EventHandler(this.HTMLponudaBrowseButton_Click);
            // 
            // textBoxDBPath
            // 
            this.textBoxDBPath.Location = new System.Drawing.Point(6, 19);
            this.textBoxDBPath.Name = "textBoxDBPath";
            this.textBoxDBPath.Size = new System.Drawing.Size(373, 20);
            this.textBoxDBPath.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.textBoxDBPath);
            this.groupBox2.Controls.Add(this.HTMLponudaBrowseButton);
            this.groupBox2.Location = new System.Drawing.Point(12, 77);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(466, 59);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DB path:";
            // 
            // groupBox5
            // 
            this.groupBox5.AutoSize = true;
            this.groupBox5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox5.Controls.Add(this.textBoxPythonPath);
            this.groupBox5.Controls.Add(this.PythonPathBrowseButton);
            this.groupBox5.Location = new System.Drawing.Point(12, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(466, 59);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "python.exe path:";
            // 
            // textBoxPythonPath
            // 
            this.textBoxPythonPath.Location = new System.Drawing.Point(6, 19);
            this.textBoxPythonPath.Name = "textBoxPythonPath";
            this.textBoxPythonPath.Size = new System.Drawing.Size(373, 20);
            this.textBoxPythonPath.TabIndex = 1;
            // 
            // PythonPathBrowseButton
            // 
            this.PythonPathBrowseButton.Location = new System.Drawing.Point(385, 17);
            this.PythonPathBrowseButton.Name = "PythonPathBrowseButton";
            this.PythonPathBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.PythonPathBrowseButton.TabIndex = 0;
            this.PythonPathBrowseButton.Text = "Browse...";
            this.PythonPathBrowseButton.UseVisualStyleBackColor = true;
            this.PythonPathBrowseButton.Click += new System.EventHandler(this.PythonPathBrowseButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.minutePicker);
            this.groupBox1.Controls.Add(this.satiPicker);
            this.groupBox1.Location = new System.Drawing.Point(12, 142);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(153, 44);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Download time of day";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(102, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "min";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(43, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "h";
            // 
            // minutePicker
            // 
            this.minutePicker.Location = new System.Drawing.Point(65, 19);
            this.minutePicker.Name = "minutePicker";
            this.minutePicker.Size = new System.Drawing.Size(38, 20);
            this.minutePicker.TabIndex = 7;
            this.minutePicker.Leave += new System.EventHandler(this.minutePicker_Leave);
            // 
            // satiPicker
            // 
            this.satiPicker.Location = new System.Drawing.Point(6, 19);
            this.satiPicker.Name = "satiPicker";
            this.satiPicker.Size = new System.Drawing.Size(38, 20);
            this.satiPicker.TabIndex = 6;
            this.satiPicker.Leave += new System.EventHandler(this.satiPicker_Leave);
            // 
            // labelStep1
            // 
            this.labelStep1.AutoSize = true;
            this.labelStep1.Location = new System.Drawing.Point(350, 150);
            this.labelStep1.Name = "labelStep1";
            this.labelStep1.Size = new System.Drawing.Size(91, 13);
            this.labelStep1.TabIndex = 7;
            this.labelStep1.Text = "Folders creation...";
            // 
            // labelStep2
            // 
            this.labelStep2.AutoSize = true;
            this.labelStep2.Location = new System.Drawing.Point(350, 173);
            this.labelStep2.Name = "labelStep2";
            this.labelStep2.Size = new System.Drawing.Size(88, 13);
            this.labelStep2.TabIndex = 7;
            this.labelStep2.Text = "Settings saving...";
            // 
            // labelStep3
            // 
            this.labelStep3.AutoSize = true;
            this.labelStep3.Location = new System.Drawing.Point(350, 194);
            this.labelStep3.Name = "labelStep3";
            this.labelStep3.Size = new System.Drawing.Size(96, 13);
            this.labelStep3.TabIndex = 7;
            this.labelStep3.Text = "Task initialization...";
            // 
            // labelStep4
            // 
            this.labelStep4.AutoSize = true;
            this.labelStep4.Location = new System.Drawing.Point(350, 216);
            this.labelStep4.Name = "labelStep4";
            this.labelStep4.Size = new System.Drawing.Size(52, 13);
            this.labelStep4.TabIndex = 7;
            this.labelStep4.Text = "Launch!!!";
            // 
            // picStep4
            // 
            this.picStep4.Location = new System.Drawing.Point(328, 213);
            this.picStep4.Name = "picStep4";
            this.picStep4.Size = new System.Drawing.Size(16, 16);
            this.picStep4.TabIndex = 6;
            this.picStep4.TabStop = false;
            // 
            // picStep3
            // 
            this.picStep3.Location = new System.Drawing.Point(328, 191);
            this.picStep3.Name = "picStep3";
            this.picStep3.Size = new System.Drawing.Size(16, 16);
            this.picStep3.TabIndex = 6;
            this.picStep3.TabStop = false;
            // 
            // picStep2
            // 
            this.picStep2.Location = new System.Drawing.Point(328, 169);
            this.picStep2.Name = "picStep2";
            this.picStep2.Size = new System.Drawing.Size(16, 16);
            this.picStep2.TabIndex = 6;
            this.picStep2.TabStop = false;
            // 
            // picStep1
            // 
            this.picStep1.Location = new System.Drawing.Point(328, 147);
            this.picStep1.Name = "picStep1";
            this.picStep1.Size = new System.Drawing.Size(16, 16);
            this.picStep1.TabIndex = 6;
            this.picStep1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 270);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 39);
            this.button1.TabIndex = 1;
            this.button1.Text = "Connect to DB and list them";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(203, 251);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(158, 58);
            this.textBox1.TabIndex = 1;
            // 
            // StartUpWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 321);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.labelStep4);
            this.Controls.Add(this.labelStep3);
            this.Controls.Add(this.labelStep2);
            this.Controls.Add(this.labelStep1);
            this.Controls.Add(this.picStep4);
            this.Controls.Add(this.picStep3);
            this.Controls.Add(this.picStep2);
            this.Controls.Add(this.picStep1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.startButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "StartUpWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "App settings...";
            this.Load += new System.EventHandler(this.StartUpWindow_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minutePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.satiPicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStep1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button HTMLponudaBrowseButton;
        private System.Windows.Forms.TextBox textBoxDBPath;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBoxPythonPath;
        private System.Windows.Forms.Button PythonPathBrowseButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown minutePicker;
        private System.Windows.Forms.NumericUpDown satiPicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox picStep1;
        private System.Windows.Forms.Label labelStep1;
        private System.Windows.Forms.PictureBox picStep2;
        private System.Windows.Forms.PictureBox picStep3;
        private System.Windows.Forms.PictureBox picStep4;
        private System.Windows.Forms.Label labelStep2;
        private System.Windows.Forms.Label labelStep3;
        private System.Windows.Forms.Label labelStep4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
    }
}