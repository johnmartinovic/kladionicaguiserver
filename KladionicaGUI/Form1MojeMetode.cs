﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Diagnostics;
using KladionicaSettings;
// za ObjectListView:
using BrightIdeasSoftware;

namespace KladionicaGUI
{
    partial class Form1
    {
        enum PoljaDogadaja : int
        {
            BrojPonude,
            Sport,
            Liga,
            Datum,
            Vrijeme,
            TipPonude,
            NazivDogadaja,
            TipKoef,
            VrijednostKoef,
            Rezultat,
            DobitniTipovi
        }

        // lista sa svim dogadajima iz baze
        List<string[]> ListaSvihDogadaja;
        // lista sa apsolutno svim dogadajima iz baze
        List<string[]> ListaApsolutnoSvihDogadaja;
        // lista sa svim dogadajima oznacenim u treeviewu
        List<string[]> Filter1ListaSvihDogadaja;
        // lista sa svim dogadajima oznacenim u treeviewu sport/liga i odds listi
        List<string[]> Filter2ListaSvihDogadaja;
        // struktura s podatcima o koeficijentu - broj utakmica, broj 1, broj X, broj 2
        class KoefPodatci
        {
            public int brojUt;
            public int broj1;
            public int brojX;
            public int broj2;
            public float posto1;
            public float postoX;
            public float posto2;
        }
        // hashtable s vrijednostima u obliku koeficijent-podatci, gdje su podatci u iznad navedenoj klasi,
        // nefiltrirani i filtrirani
        Dictionary<string, KoefPodatci> DictKoeficijenti;
        Dictionary<string, KoefPodatci> FilteredDictKoeficijenti;

        // path do XML dokumenta s postavkama programa
        string XMLPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) +
                "\\KladionicaDATA.xml";

        // path do programa koji sluzi za skidanje podataka kako
        // bi se mogao staviti u Windows Task Scheduler
        string downloadingEXEPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) +
                "\\KladionicaDataDownloader.exe";

        // globalne varijable koje su izvucene iz XML-a s postavkama programa
        // pri pokretanju ovog prozora
        string globalPythonPath = "";
        string globalDatabasePath = "";
                
        // BackgroundWorker koji ce sluziti za skidanje podataka s interneta
        BackgroundWorker bwObrada;

        /// <summary>
        /// Inicijalizacija kucica u prozoru
        /// </summary>
        void InitForm1()
        {
            BWObradaInitialization();

            // inicijalizacija notification ikone
            notifyIcon1.BalloonTipText = "Klikni dvaput za glavni prozor.";
            notifyIcon1.BalloonTipTitle = "Minimizirano!";
            notifyIcon1.Text = "Klikni dvaput za glavni prozor.";

            // inicijalizacija varijabli da program ne pada kad se kliknu gumbi jer varijable prazne
            this.ListaSvihDogadaja = new List<string[]>();
            this.ListaApsolutnoSvihDogadaja = new List<string[]>();
            this.Filter1ListaSvihDogadaja = new List<string[]>();
            this.Filter2ListaSvihDogadaja = new List<string[]>();

            // inicijalizacija datuma na danasnji datum
            fromDateTimePicker.Value = DateTime.Today;
            toDateTimePicker.Value = DateTime.Today;

            // sakrij label i loading slikicu i postavi slikicu u loading
            this.picSteps.Visible = false;
            this.labelSteps.Visible = false;
            this.picSteps.Image = Properties.Resources.Loading;

            InitOddsObjectListView();

            InitMatchListView();

            InitStatsObjectListView();

            // inicijalizacija Sports/Leagues tablice
            SportsLeaguesTreeView.CheckBoxes = true;
            SportsLeaguesTreeView.ShowNodeToolTips = true;
        }

        void InitMatchListView()
        {
            // inicijalizacija MatchList tablice
            MatchListView.Columns.Add("Datum");
            MatchListView.Columns.Add("Sport");
            MatchListView.Columns.Add("Liga");
            MatchListView.Columns.Add("Par");
            MatchListView.Columns.Add("Rezultat");
            MatchListView.View = View.Details;
            MatchListView.GridLines = true;
            MatchListView.FullRowSelect = true;
        }

        void InitOddsObjectListView()
        {
            // inicijalizacija OddsObject tablice
            List<OLVColumn> oddsColumnList = new List<OLVColumn>();
            OLVColumn oddsOlvColumn1 = new OLVColumn()
            {
                Text = "Koeficijent",
                AspectGetter = obj => (obj as rowOddsObjectListView).Koef
            };
            OLVColumn oddsOlvColumn2 = new OLVColumn()
            {
                Text = "No.",
                AspectGetter = obj => (obj as rowOddsObjectListView).brojUt
            };
            OLVColumn oddsOlvColumn3 = new OLVColumn()
            {
                Text = "1",
                AspectGetter = obj => (obj as rowOddsObjectListView).broj1
            };
            OLVColumn oddsOlvColumn4 = new OLVColumn()
            {
                Text = "1(%)",
                AspectGetter = obj => (obj as rowOddsObjectListView).posto1,
                AspectToStringFormat = "{0:F1}"
            };
            OLVColumn oddsOlvColumn5 = new OLVColumn()
            {
                Text = "X",
                AspectGetter = obj => (obj as rowOddsObjectListView).brojX
            };
            OLVColumn oddsOlvColumn6 = new OLVColumn()
            {
                Text = "X(%)",
                AspectGetter = obj => (obj as rowOddsObjectListView).postoX,
                AspectToStringFormat = "{0:F1}"
            };
            OLVColumn oddsOlvColumn7 = new OLVColumn()
            {
                Text = "2",
                AspectGetter = obj => (obj as rowOddsObjectListView).broj2
            };
            OLVColumn oddsOlvColumn8 = new OLVColumn()
            {
                Text = "2(%)",
                AspectGetter = obj => (obj as rowOddsObjectListView).posto2,
                AspectToStringFormat = "{0:F1}"
            };
            OddsObjectListView.AllColumns.AddRange(
                new List<OLVColumn>(){oddsOlvColumn1, oddsOlvColumn2, oddsOlvColumn3, oddsOlvColumn4,
                    oddsOlvColumn5, oddsOlvColumn6, oddsOlvColumn7, oddsOlvColumn8}
            );
            OddsObjectListView.RebuildColumns();
            OddsObjectListView.ShowGroups = false;
            OddsObjectListView.FullRowSelect = true;
            OddsObjectListView.CheckBoxes = true;
            OddsObjectListView.CustomSorter = delegate(OLVColumn column, SortOrder order)
            {
                if (column.Text == "1" || column.Text == "1(%)" || column.Text == "X" ||
                    column.Text == "X(%)" || column.Text == "2" || column.Text == "2(%)")
                    this.OddsObjectListView.ListViewItemSorter = new ColumnComparer(
                        column, order,
                        oddsOlvColumn2, SortOrder.Descending);
                else if (column.Text == "No.")
                    this.OddsObjectListView.ListViewItemSorter = new ColumnComparer(
                        column, order,
                        oddsOlvColumn1, SortOrder.Descending);
                else
                    this.OddsObjectListView.ListViewItemSorter = new ColumnComparer(
                       column, order);
            };
        }

        void InitStatsObjectListView()
        {
            // inicijalizacija StatsObject tablice
            List<OLVColumn> statsColumnList = new List<OLVColumn>();
            OLVColumn statsOlvColumn1 = new OLVColumn()
            {
                Text = "Tip",
                AspectGetter = obj => (obj as rowStatsObjectListView).Tip
            };
            OLVColumn statsOlvColumn2 = new OLVColumn()
            {
                Text = "Stat.",
                AspectGetter = obj => (obj as rowStatsObjectListView).statTipa
            };
            OLVColumn statsOlvColumn3 = new OLVColumn()
            {
                Text = "Stat.(%)",
                AspectGetter = obj => (obj as rowStatsObjectListView).statTipaPosto,
                AspectToStringFormat = "{0:F2}"
            };
            StatsObjectListView.AllColumns.AddRange(
                new List<OLVColumn>() { statsOlvColumn1, statsOlvColumn2, statsOlvColumn3 }
            );
            StatsObjectListView.RebuildColumns();
            StatsObjectListView.ShowGroups = false;
            StatsObjectListView.FullRowSelect = true;
        }

        /// <summary>
        /// Metoda koja izraduje ListaSvihDogadaja i ListaApsolutnoSvihDogadaja iz zadanog patha
        /// </summary>
        void ListaSvihDogadajaGenerator()
        {
            this.ListaSvihDogadaja = new List<string[]>();
            this.ListaApsolutnoSvihDogadaja = new List<string[]>();
            DateTime tempFromDate = fromDateTimePicker.Value;
            DateTime tempToDate = toDateTimePicker.Value;

            string[] listaComboFileova = Directory.GetFiles(globalDatabasePath + @"\SupersportComboPonRez");
            foreach (string comboFilePath in listaComboFileova){
                string comboFileName = Path.GetFileName(comboFilePath);
                DateTime datumComboFileName = DateTime.Parse(comboFileName.Split('_')[1].Split('.')[0]);

                // Uzmi samo dogadaje di je datum unatar zadanog
                if (datumComboFileName >= tempFromDate &&
                    datumComboFileName <= tempToDate)
                {
                    using (StreamReader reader = new StreamReader(comboFilePath, System.Text.Encoding.Default))
                    {
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            string[] dogadaj = line.Split('|');
                            // Filtriraj dogadaje, ne zelimo ih sve
                            if (dogadaj[(int)PoljaDogadaja.Sport] == "Nogomet" &&
                                dogadaj[(int)PoljaDogadaja.Rezultat] != "storno" &&
                                !dogadaj[(int)PoljaDogadaja.Rezultat].Contains("pen.") &&
                                !dogadaj[(int)PoljaDogadaja.Rezultat].Contains("prod."))
                            {
                                this.ListaApsolutnoSvihDogadaja.Add(dogadaj);
                                if (dogadaj[(int)PoljaDogadaja.TipPonude] == "OSNOVNA")
                                    this.ListaSvihDogadaja.Add(dogadaj);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Metoda koja izraduje sadržaj prozora SportsLeaguesTreeView iz ListaSvihDogadaja
        /// </summary>
        void SportsLeaguesTreeViewGenerator()
        {
            string tempImeSporta, tempImeLige;
            TreeNode tnSport, tnLiga;
            TreeNode[] tns;

            foreach (string[] tempDogadaj in this.ListaSvihDogadaja)
            {
                // Provjera postoji li sport u koji spada dogadaj u treeViewu.
                // Ako ne, dodaj ga!
                tempImeSporta = tempDogadaj[(int)PoljaDogadaja.Sport];
                tempImeLige = tempDogadaj[(int)PoljaDogadaja.Liga];

                if (!SportsLeaguesTreeView.Nodes.ContainsKey(tempImeSporta))
                {
                    tnSport = new TreeNode(tempImeSporta);
                    tnSport.Name = tnSport.ToolTipText = tempImeSporta;
                    tnLiga = new TreeNode(tempImeLige);
                    tnLiga.Name = tnLiga.ToolTipText = tempImeLige;

                    tnSport.Nodes.Add(tnLiga);
                    SportsLeaguesTreeView.Nodes.Add(tnSport);
                }
                else
                {
                    tns = SportsLeaguesTreeView.Nodes.Find(tempImeSporta, false);
                    if (!tns[0].Nodes.ContainsKey(tempImeLige))
                    {
                        tnLiga = new TreeNode(tempImeLige);
                        tnLiga.Name = tnLiga.ToolTipText = tempImeLige;

                        tns[0].Nodes.Add(tnLiga);
                    }
                }
            }

            // sortiraj ovaj SportsLeaguesTreeView
            SportsLeaguesTreeView.Sort();
        }

        /// <summary>
        /// Metoda koja stvara novu listu onih dogadaja koji su checkirani u SportsLeaguesTreeView
        /// </summary>
        void Filter1ListaSvihDogadajaGenerator()
        {
            this.Filter1ListaSvihDogadaja = new List<string[]>();

            foreach (string[] dogadaj in this.ListaSvihDogadaja)
            {
                if (OznacenUStablu(dogadaj))
                    this.Filter1ListaSvihDogadaja.Add(dogadaj);
            }
        }

        /// <summary>
        /// Pomocna metoda od metode Filter1SportLiga
        /// </summary>
        /// <param name="dogadaj"></param>
        /// <returns></returns>
        Boolean OznacenUStablu(string[] dogadaj)
        {
            foreach (TreeNode tn0 in SportsLeaguesTreeView.Nodes)
                if (tn0.Text == dogadaj[(int)PoljaDogadaja.Sport])
                {
                    foreach (TreeNode tn1 in tn0.Nodes)
                        if (tn1.Text == dogadaj[(int)PoljaDogadaja.Liga] && tn1.Checked)
                            return true;
                    return false;
                }
            return false;
        }

        /// <summary>
        /// Metoda koja izraduje DictKoeficijenti iz Filter1ListaSvihDogadaja
        /// OddsListView tablice
        /// </summary>
        void DictKoeficijentiGenerator()
        {
            this.DictKoeficijenti = new Dictionary<string, KoefPodatci>();

            KoefPodatci tempKoefPodatci = new KoefPodatci();
            string[] tempKoeficijenti;
            string noviKoef;
            string[] tempRez;

            foreach (string[] dogadaj in Filter1ListaSvihDogadaja)
            {
                tempKoeficijenti = dogadaj[(int)PoljaDogadaja.VrijednostKoef].Split(' ');
                noviKoef = tempKoeficijenti[0] + " " + tempKoeficijenti[1] + " " + tempKoeficijenti[2];
                tempRez = dogadaj[(int)PoljaDogadaja.Rezultat].Split(':');

                // dodavanje novog koeficijenta (ili samo uvecavanje broja tih istih koeficijenata)
                if (this.DictKoeficijenti.ContainsKey(noviKoef))
                    this.DictKoeficijenti[noviKoef].brojUt++;
                else
                    this.DictKoeficijenti.Add(noviKoef, new KoefPodatci(){brojUt=1, broj1=0, broj2=0, brojX=0});
                
                // azuriranje broja jedinica, dvojki, x-eva za pojedini koeficijent
                if (int.Parse(tempRez[0].Trim().Split(' ')[0]) > int.Parse(tempRez[1].Trim().Split(' ')[0]))
                    this.DictKoeficijenti[noviKoef].broj1++;
                else if (int.Parse(tempRez[0].Trim().Split(' ')[0]) < int.Parse(tempRez[1].Trim().Split(' ')[0]))
                    this.DictKoeficijenti[noviKoef].broj2++;
                else
                    this.DictKoeficijenti[noviKoef].brojX++;
            }

            foreach (KeyValuePair<string, KoefPodatci> kvp in this.DictKoeficijenti)
            {
                kvp.Value.posto1 = (float)kvp.Value.broj1 / kvp.Value.brojUt * 100;
                kvp.Value.postoX = (float)kvp.Value.brojX / kvp.Value.brojUt * 100;
                kvp.Value.posto2 = (float)kvp.Value.broj2 / kvp.Value.brojUt * 100;
            }

            // filtriranje dictionarya s obzirom na vrijednosti u Odds options dijelu
            this.FilteredDictKoeficijenti = new Dictionary<string, KoefPodatci>();

            foreach (KeyValuePair<string, KoefPodatci> kvp in this.DictKoeficijenti)
            {
                string[] koefs = kvp.Key.Split(' ');

                // preskoci clan iz dictionarya 
                if (!koefs[0].StartsWith(filterOdds1TextBox.Text) ||
                    !koefs[1].StartsWith(filterOddsXTextBox.Text) ||
                    !koefs[2].StartsWith(filterOdds2TextBox.Text) ||
                    kvp.Value.brojUt < minGamesPicker.Value)
                    continue;

                this.FilteredDictKoeficijenti.Add(kvp.Key, kvp.Value);
            }
        }

        /// <summary>
        /// Metoda koja izraduje sadržaj prozora OddsObjectListView iz DictKoeficijenti
        /// </summary>
        void OddsObjectListViewGenerator()
        {
            // Clear pa ubaci vrijednosti u OddsObjectListView
            OddsObjectListView.ClearObjects();
            List<rowOddsObjectListView> rows = new List<rowOddsObjectListView>();
            foreach (KeyValuePair<string, KoefPodatci> kvp in this.FilteredDictKoeficijenti)
            {
                rows.Add(new rowOddsObjectListView(kvp.Key, kvp.Value.brojUt, 
                                    kvp.Value.broj1, kvp.Value.posto1,
                                    kvp.Value.brojX, kvp.Value.postoX,
                                    kvp.Value.broj2, kvp.Value.posto2));
            }
            OddsObjectListView.SetObjects(rows);
            OddsObjectListView.AutoResizeColumns();
        }

        // row klasa za OddsObjectListView
        class rowOddsObjectListView
        {
            public string Koef;
            public int brojUt;
            public int broj1;
            public float posto1;
            public int brojX;
            public float postoX;
            public int broj2;
            public float posto2;

            public rowOddsObjectListView(string Koef, int brojUt, int broj1, float posto1, int brojX,
                float postoX, int broj2, float posto2)
            {
                this.Koef = Koef;
                this.brojUt = brojUt;
                this.broj1 = broj1;
                this.posto1 = posto1;
                this.brojX = brojX;
                this.postoX = postoX;
                this.broj2 = broj2;
                this.posto2 = posto2;
            }
        }

        /// <summary>
        /// Metoda koja izraduje sadržaj prozora StatsObjectListView iz Filter2ListaSvihDogadaja
        /// </summary>
        void StatsObjectListViewGenerator()
        {
            // Ubaci vrijednosti u StatsObjectListView
            List<rowStatsObjectListView> rows = new List<rowStatsObjectListView>(36);
            List<helpRowStatsObjectListView> helpRows = new List<helpRowStatsObjectListView>(36);
            for (int i = 0; i < 36; i++)
                helpRows.Add(new helpRowStatsObjectListView());

            string tempDatum, tempBrojPonude, tempRez;
            string[] tempRow;
            string[] tempStr;
            int tempRez1, tempRez2;

            foreach (string[] dogadaj in this.Filter2ListaSvihDogadaja)
            {
                tempDatum = dogadaj[(int)PoljaDogadaja.Datum];
                tempBrojPonude = dogadaj[(int)PoljaDogadaja.BrojPonude];

                // trazenje slucaja 0, 1, 2
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "1");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.Rezultat];
                    tempStr = tempRez.Split(':');

                    if (Int32.Parse(tempStr[0]) > Int32.Parse(tempStr[1]))
                        helpRows[0].broj++;
                    else if (Int32.Parse(tempStr[0]) == Int32.Parse(tempStr[1]))
                        helpRows[1].broj++;
                    else helpRows[2].broj++;

                    // ovdje bi se moglo malo optimizirati, tipa da se drugi red prebaci skroz na kraj
                    helpRows[0].ukBroj++;
                    helpRows[1].ukBroj++;
                    helpRows[2].ukBroj++;
                }

                // trazenje slucaja 3, 4, 5
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "2");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.Rezultat];
                    tempStr = tempRez.Split(':');

                    if (Int32.Parse(tempStr[0]) > Int32.Parse(tempStr[1]))
                        helpRows[3].broj++;
                    else if (Int32.Parse(tempStr[0]) == Int32.Parse(tempStr[1]))
                        helpRows[4].broj++;
                    else helpRows[5].broj++;

                    // ovdje bi se moglo malo optimizirati, tipa da se drugi red prebaci skroz na kraj
                    helpRows[3].ukBroj++;
                    helpRows[4].ukBroj++;
                    helpRows[5].ukBroj++;
                }

                // trazenje slucaja 6
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "3");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "Daje")
                        helpRows[6].broj++;

                    helpRows[6].ukBroj++;
                }

                // trazenje slucaja 7
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "4");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "Daje")
                        helpRows[7].broj++;

                    helpRows[7].ukBroj++;
                }

                // trazenje slucaja 8, 9
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "66");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "11")
                        helpRows[8].broj++;
                    else if (tempRez == "22")
                        helpRows[9].broj++;

                    // ovdje bi se moglo malo optimizirati, tipa da se drugi red prebaci skroz na kraj
                    helpRows[8].ukBroj++;
                    helpRows[9].ukBroj++;
                }

                // trazenje slucaja 10
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "41");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "da")
                        helpRows[10].broj++;

                    helpRows[10].ukBroj++;
                }

                // trazenje slucaja 11
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "42");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "da")
                        helpRows[11].broj++;

                    helpRows[11].ukBroj++;
                }

                // trazenje slucaja 12
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "48");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "da")
                        helpRows[12].broj++;

                    helpRows[12].ukBroj++;
                }

                // trazenje slucaja 13
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "58");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "da")
                        helpRows[13].broj++;

                    helpRows[13].ukBroj++;
                }

                // trazenje slucaja 14
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "62");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "da")
                        helpRows[14].broj++;

                    helpRows[14].ukBroj++;
                }

                // trazenje slucaja 15 - 26, 33
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude);

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.Rezultat];
                    tempStr = tempRez.Split(':');
                    tempRez1 = Int32.Parse(tempStr[0]);
                    tempRez2 = Int32.Parse(tempStr[1]);

                    if (tempRez1 > tempRez2 + 1)
                        helpRows[15].broj++;
                    else if (tempRez1 == tempRez2 + 1)
                        helpRows[16].broj++;
                    else helpRows[17].broj++;

                    if (tempRez1 > tempRez2 + 2)
                        helpRows[18].broj++;
                    else if (tempRez1 == tempRez2 + 2)
                        helpRows[19].broj++;
                    else helpRows[20].broj++;

                    if (tempRez1 + 1 > tempRez2)
                        helpRows[21].broj++;
                    else if (tempRez1 + 1 == tempRez2)
                        helpRows[22].broj++;
                    else helpRows[23].broj++;

                    if (tempRez1 + 2 > tempRez2)
                        helpRows[24].broj++;
                    else if (tempRez1 + 2 == tempRez2)
                        helpRows[25].broj++;
                    else helpRows[26].broj++;

                    // ovdje bi se moglo malo optimizirati, tipa da se drugi red prebaci skroz na kraj
                    for (int i = 15; i < 27; i++)
                        helpRows[i].ukBroj++;

                    // dio za prosjek golova
                    helpRows[33].broj += (tempRez1 + tempRez2);
                    helpRows[33].ukBroj++;
                }

                // trazenje slucaja 27
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "6");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "da")
                        helpRows[27].broj++;

                    helpRows[27].ukBroj++;
                }

                // trazenje slucaja 28
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "60");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "da")
                        helpRows[28].broj++;

                    helpRows[28].ukBroj++;
                }

                // trazenje slucaja 29
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "7");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez == "da")
                        helpRows[29].broj++;

                    helpRows[29].ukBroj++;
                }

                // trazenje slucaja 30, 31, 32
                tempRow = ListaApsolutnoSvihDogadaja.FirstOrDefault(x =>
                    x[(int)PoljaDogadaja.Datum] == tempDatum &&
                    x[(int)PoljaDogadaja.BrojPonude] == tempBrojPonude + "49");

                if (tempRow != null)
                {
                    tempRez = tempRow[(int)PoljaDogadaja.DobitniTipovi];

                    if (tempRez.Contains("1"))
                        helpRows[30].broj++;
                    if (tempRez.Contains("X"))
                        helpRows[31].broj++;
                    if (tempRez.Contains("2"))
                        helpRows[32].broj++;

                    // ovdje bi se moglo malo optimizirati, tipa da se drugi red prebaci skroz na kraj
                    helpRows[30].ukBroj++;
                    helpRows[31].ukBroj++;
                    helpRows[32].ukBroj++;
                }
            }

            for (int i = 0; i < 34; i++)
            {
                rows.Add(new rowStatsObjectListView(this.listaStatistika[i],
                helpRows[i].broj.ToString() + "/" + helpRows[i].ukBroj.ToString(),
                (float)helpRows[i].broj / (float)helpRows[i].ukBroj));
            }

            StatsObjectListView.SetObjects(rows);
            StatsObjectListView.AutoResizeColumns();
        }

        // Lista statistika ce biti (u zagradi su poveznice za dogadaj s brojem 349):
        string[] listaStatistika = new string[34]{
            "1.p.H.pob.",       // 0 - 1. pol. Home pobjeda (3491)
            "1.p.X",            // 1 - 1. pol. X (3491)
            "1.p.A.pob.",       // 2 - 1. pol. Away pobjeda (3491)
            "2.p.H.pob.",       // 3 - 2. pol. Home pobjeda (3492)
            "2.p.X",            // 4 - 2. pol. X (3492)
            "2.p.A.pob.",       // 5 - 2. pol. Away pobjeda (3492)
            "Home daje gol",    // 6 - Home daje gol (3493)
            "Away daje gol",    // 7 - Away daje gol (3494)
            "Home dv. pob.",    // 8 - Home dvostruka pobjeda (34966)
            "Away dv. pob.",    // 9 - Away dvostruka pobjeda (34966)
            "Home GObaPol",     // 10 - Home gol u oba pol. (34941)
            "Away GObaPol",     // 11 - Away gol u oba pol. (34942)
            "Gol u 1. pol.",    // 12 - gol u 1. pol. (34948)
            "Gol u 2. pol.",    // 13 - gol u 2. pol. (34958)
            "Gol u oba pol.",   // 14 - gol u oba pol. (34962)
            // ***************************hendikepi nemaju isti nadodani broj pa se za njih analizira
            // ***************************bas rezultat
            "H.pob.(0:1)",      // 15 - hendikep(0:1) Home pobjeda (34900)
            "X (0:1)",          // 16 - hendikep(0:1) X (34900)
            "A.pob.(0:1)",      // 17 - hendikep(0:1) Away pobjeda (34900)
            "H.pob.(0:2)",      // 18 - hendikep(0:2) Home pobjeda (34901)
            "X (0:2)",          // 19 - hendikep(0:2) X (34901)
            "A.pob.(0:2)",      // 20 - hendikep(0:2) Away pobjeda (34901)
            "H.pob.(1:0)",      // 21 - hendikep(1:0) Home pobjeda (34902)
            "X (1:0)",          // 22 - hendikep(1:0) X (34902)
            "A.pob.(1:0)",      // 23 - hendikep(1:0) Away pobjeda (34902)
            "H.pob.(2:0)",      // 24 - hendikep(2:0) Home pobjeda (34903)
            "X (2:0)",          // 25 - hendikep(2:0) X (34903)
            "A.pob.(2:0)",      // 26 - hendikep(2:0) Away pobjeda (34903)
            "ObaD.gol",         // 27 - oba daju gol (3496)
            "ObaD.gol 1.p.",    // 28 - oba daju gol 1. pol. (34960)
            "ObaD.goli3+",      // 29 - oba daju gol i 3+ (3497)
            "PiliK H.pob.",     // 30 - pol. ili kraj Home pobjeda (34949)
            "PiliK X",          // 31 - pol. ili kraj X (34949)
            "PiliK A.pob.",     // 32 - pol. ili kraj Away pobjeda (34949)
            "Prosjek gol."      // 33 - prosjek golova
        };

        // row klasa za StatsObjectListView
        class rowStatsObjectListView
        {
            public string Tip;
            public string statTipa;
            public float statTipaPosto;

            public rowStatsObjectListView(string Tip, string statTipa, float statTipaPosto)
            {
                this.Tip = Tip;
                this.statTipa = statTipa;
                this.statTipaPosto = statTipaPosto;
            }
        }
        class helpRowStatsObjectListView
        {
            public int broj;
            public int ukBroj;

            public helpRowStatsObjectListView()
            {
                this.broj = 0;
                this.ukBroj = 0;
            }
        }
        
        /// <summary>
        /// metoda koja izraduje listu dogadaja koji su u checkirani u OddsObjectListView-u tako
        /// da preuzima dogadaje iz Filter1ListaSvihDogadaja
        /// </summary>
        void Filter2ListaSvihDogadajaGenerator()
        {
            this.Filter2ListaSvihDogadaja = new List<string[]>();

            foreach (rowOddsObjectListView checkedKoef in OddsObjectListView.CheckedObjects)
            {
                foreach (string[] dogadaj in Filter1ListaSvihDogadaja)
                {
                    string[] tempKoeficijenti = dogadaj[(int)PoljaDogadaja.VrijednostKoef].Split(' ');
                    string noviKoef = tempKoeficijenti[0] + " " + tempKoeficijenti[1] + " " + tempKoeficijenti[2];
                    if (noviKoef == checkedKoef.Koef)
                        this.Filter2ListaSvihDogadaja.Add(dogadaj);
                }
            }
        }

        /// <summary>
        /// Metoda koja izraduje sadržaj prozora MatchListView iz Filter2ListaSvihDogadaja
        /// </summary>
        void MatchListViewGenerator()
        {
            foreach (string[] dogadaj in this.Filter2ListaSvihDogadaja)
            {
                string[] temp1 = {  dogadaj[(int) PoljaDogadaja.Datum],
                                    dogadaj[(int) PoljaDogadaja.Sport],
                                    dogadaj[(int) PoljaDogadaja.Liga],
                                    dogadaj[(int) PoljaDogadaja.NazivDogadaja],
                                    dogadaj[(int) PoljaDogadaja.Rezultat]};
                // Ubaci vrijednosti u MatchListView
                MatchListView.Items.Add(new ListViewItem(temp1));
            }

            MatchListView.Columns[0].Width = -1;
            MatchListView.Columns[1].Width = -1;
            MatchListView.Columns[2].Width = -1;
            MatchListView.Columns[3].Width = -1;
        }

        /// <summary>
        /// Metoda koja rekurzivno checkira SVE nodeove proslijedene u parametru
        /// </summary>
        /// <param name="nodes">Nodeovi koji ce se checkirati</param>
        private void CheckAllCheckBoxesTreeView(TreeNodeCollection nodes, Boolean checking)
        {
            foreach (TreeNode tn in nodes)
            {
                tn.Checked = checking;
                if (tn.Nodes.Count != 0)
                    CheckAllCheckBoxesTreeView(tn.Nodes, checking);
            }
        }

        /// <summary>
        /// Pomoćna metoda od SportsLeaguesTreeView_AfterCheck
        /// koja rekurzivno checkira/UNcheckira sve subnodeove od zadanog nodea
        /// </summary>
        /// <param name="treeNode">zadani node</param>
        /// <param name="nodeChecked">true ako se checkiraju, a false ako se UNcheckiraju</param>
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    // If the current node has child nodes, call the CheckAllChildsNodes method recursively. 
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }

        /// <summary>
        /// Inicijalizacija BackgroundWorkera za obradu podataka
        /// </summary>
        void BWObradaInitialization()
        {
            this.bwObrada = new BackgroundWorker();

            // this allows our worker to report progress during work
            this.bwObrada.WorkerReportsProgress = true;

            // what to do in the background thread
            this.bwObrada.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    BackgroundWorker b = o as BackgroundWorker;

                    

                    b.ReportProgress(0, "Bids proccessing...");

                    // opce postavke
                    string tempPythonKod;
                    string tempFile;

                    Process process = new Process();
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.FileName = this.globalPythonPath; //full path to python.exe

                    // postavke za skriptu za obradu ponude
                    tempPythonKod = KladionicaSettings.Properties.Resources.HTMLObradaPonudaString;
                    tempFile = Path.GetTempFileName();
                    using (var writer = new StreamWriter(tempFile, false))
                        writer.Write(tempPythonKod);

                    process.StartInfo.Arguments = "\"" + tempFile + "\"" + " " +
                        "\"" + this.globalDatabasePath + @"\SupersportHTMLPonuda" + "\"" + " " +
                        "\"" + this.globalDatabasePath + @"\SupersportObradenoPonuda" + "\""; //args is path to .py file and any cmd line args

                    // MessageBox.Show(process.StartInfo.Arguments);

                    process.Start();
                    process.WaitForExit();



                    b.ReportProgress(1, "Results proccessing...");
                    
                    // postavke za skriptu za obradu rezultata
                    tempPythonKod = KladionicaSettings.Properties.Resources.HTMLObradaRezultatiString;
                    tempFile = Path.GetTempFileName();
                    using (var writer = new StreamWriter(tempFile, false))
                        writer.Write(tempPythonKod);

                    process.StartInfo.Arguments = "\"" + tempFile + "\"" + " " +
                        "\"" + this.globalDatabasePath + @"\SupersportHTMLRezultati" + "\"" + " " +
                        "\"" + this.globalDatabasePath + @"\SupersportObradenoRezultati" + "\""; //args is path to .py file and any cmd line args

                    // MessageBox.Show(process.StartInfo.Arguments);

                    process.Start();
                    process.WaitForExit();



                    b.ReportProgress(2, "Combining...");

                    // postavke za skriptu za obradu rezultata
                    tempPythonKod = KladionicaSettings.Properties.Resources.HTMLComboPonRezString;
                    tempFile = Path.GetTempFileName();
                    using (var writer = new StreamWriter(tempFile, false))
                        writer.Write(tempPythonKod);

                    process.StartInfo.Arguments = "\"" + tempFile + "\"" + " " +
                        "\"" + this.globalDatabasePath + @"\SupersportObradenoPonuda" + "\"" + " " +
                        "\"" + this.globalDatabasePath + @"\SupersportObradenoRezultati" + "\"" + " " +
                        "\"" + this.globalDatabasePath + @"\SupersportComboPonRez" + "\""; //args is path to .py file and any cmd line args

                    // MessageBox.Show(process.StartInfo.Arguments);

                    process.Start();
                    process.WaitForExit();



                    b.ReportProgress(3, "Done!");
                });

            this.bwObrada.ProgressChanged += new ProgressChangedEventHandler(
                delegate(object o, ProgressChangedEventArgs args)
                {
                    string faza = args.UserState as string;

                    if (faza == "Bids proccessing...")
                    {
                        // disable all controls except those two
                        foreach (Control c in this.Controls)
                        {
                            c.Enabled = false;
                        }
                        this.labelSteps.Enabled = true;
                        this.picSteps.Enabled = true;

                        // sada prikazi sliku i label
                        this.picSteps.Visible = true;
                        this.labelSteps.Visible = true;
                    }
                    else if (faza == "Done!")
                    {
                        // enable all controls
                        foreach (Control c in this.Controls)
                        {
                            c.Enabled = true;
                        }

                        // sada sakrij sliku i label
                        this.picSteps.Visible = false;
                        this.labelSteps.Visible = false;
                    }

                    // postavi tekst u skladu s fazom
                    this.labelSteps.Text = faza;
                    
                });

        }
    }
}
