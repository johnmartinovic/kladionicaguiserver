﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Diagnostics;

namespace KladionicaGUI
{
    public partial class Form1 : Form
    {
        public Form1(string temp1, string temp2)
        {
            InitializeComponent();
            
            // ovo se vuce iz startup forme
            this.globalPythonPath = temp1;
            this.globalDatabasePath = temp2;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitForm1();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                ShowInTaskbar = false;
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(1000);
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowInTaskbar = true;
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void SportsLeaguesTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // ako checkirani node ima djece onda checkiraj i njih
            if (e.Node.Nodes.Count > 0)
                this.CheckAllChildNodes(e.Node, e.Node.Checked);
        }

        private void checkAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // nađi koji control je aktivirao contextMenuStrip pa izvrši akcije ovisno o slučaju
            if (SportsLeaguesTreeView.Equals(((sender as ToolStripItem).Owner as ContextMenuStrip).SourceControl))
                CheckAllCheckBoxesTreeView(SportsLeaguesTreeView.Nodes, true);
            else if (OddsObjectListView.Equals(((sender as ToolStripItem).Owner as ContextMenuStrip).SourceControl))
                if (OddsObjectListView.GetItemCount() != 0)
                    OddsObjectListView.CheckAll();
        }

        private void uncheckAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // nađi koji control je aktivirao contextMenuStrip pa izvrši akcije ovisno o slučaju
            if (SportsLeaguesTreeView.Equals(((sender as ToolStripItem).Owner as ContextMenuStrip).SourceControl))
                CheckAllCheckBoxesTreeView(SportsLeaguesTreeView.Nodes, false);
            else if (OddsObjectListView.Equals(((sender as ToolStripItem).Owner as ContextMenuStrip).SourceControl))
                if (OddsObjectListView.GetItemCount() != 0)
                    OddsObjectListView.UncheckAll();
        }

        private void button2Korak_Click(object sender, EventArgs e)
        {
            // Počisti Viewove
            SportsLeaguesTreeView.Nodes.Clear();
            OddsObjectListView.ClearObjects();
            MatchListView.Items.Clear();
            StatsObjectListView.ClearObjects();

            ListaSvihDogadajaGenerator();
            SportsLeaguesTreeViewGenerator();
        }

        private void button3Korak_Click(object sender, EventArgs e)
        {
            // Počisti Viewove
            OddsObjectListView.ClearObjects();
            MatchListView.Items.Clear();
            StatsObjectListView.ClearObjects();

            // Izradi filtrirane varijable
            Filter1ListaSvihDogadajaGenerator();
            DictKoeficijentiGenerator();
            OddsObjectListViewGenerator();
        }

        private void button4Korak_Click(object sender, EventArgs e)
        {
            // Počisti Viewove
            MatchListView.Items.Clear();
            StatsObjectListView.ClearObjects();

            Filter2ListaSvihDogadajaGenerator();
            MatchListViewGenerator();
            StatsObjectListViewGenerator();
        }

        private void buttonObradiBazu_Click(object sender, EventArgs e)
        {
            this.bwObrada.RunWorkerAsync();
        }

        private void minGamesPicker_Leave(object sender, EventArgs e)
        {
            if (minGamesPicker.Text.Equals(""))
            {
                minGamesPicker.Text = "0";
                minGamesPicker.Value = 0;
            } 
        }

        private void buttonClearOddsOpt_Click(object sender, EventArgs e)
        {
            filterOdds1TextBox.Clear();
            filterOddsXTextBox.Clear();
            filterOdds2TextBox.Clear();
            minGamesPicker.Value = 0;
        }
    }
}
